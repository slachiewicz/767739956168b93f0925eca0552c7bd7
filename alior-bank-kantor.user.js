// ==UserScript==
// @name        AliorBank Kantor
// @namespace   glorpen
// @include     https://kantor.aliorbank.pl/login/password
// @version     1
// @grant       none
// ==/UserScript==

var oldInput = document.getElementById("masked-password");
var input = document.createElement("input");

input.style.width = oldInput.offsetWidth;
oldInput.style.display = "none";
input.id = "alior-helper";
input.type="password";

oldInput.parentNode.appendChild(input);

var fields = Array.prototype.slice.call(oldInput.getElementsByTagName("input"));

input.onchange = function(){
    for(var k in fields){
        var pos = fields[k].getAttribute("id").substring(9);
        if(pos && fields[k].getAttribute("type") == "password" && fields[k].getAttribute("class").indexOf("disabled") == -1){
          fields[k].setAttribute("value", input.value.substr(pos-1,1));
        }
    }
};